package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    HashMap<String,Double> bookPrices = new HashMap<>();

    public Quoter() {
        this.bookPrices.put("1",10.0);
        this.bookPrices.put("2",45.0);
        this.bookPrices.put("3",20.0);
        this.bookPrices.put("4",35.0);
        this.bookPrices.put("5",50.0);
    }

    double getBookPrice(String isbn){
        if(bookPrices.containsKey(isbn)) {
            return bookPrices.get(isbn);
        }
        return 0;
    }
}
